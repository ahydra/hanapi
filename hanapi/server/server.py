from collections import defaultdict
import datetime
from math import floor
from secrets import token_hex
import textwrap
from threading import Lock, Thread
import time
from typing import Callable, Optional

from flask import Flask, request

from hanapi.dataclasses.card import CardColour, CardKnowledge
from hanapi.dataclasses.exceptions import InvalidMoveError, OutOfTokensError
from hanapi.dataclasses.game import Game, GameStatus, Player

GAME_TIMEOUT_MINUTES = 60

HttpReturnType = tuple[str, int]


class GameDatabase:
    lock: Lock
    next_game_id: int
    games: dict[int, Game]
    player_token_to_player: dict[str, Player]
    player_token_to_game: dict[str, Game]

    def __init__(self) -> None:
        self.lock = Lock()
        self.games = {}
        self.next_game_id = 1
        self.player_token_to_player = {}
        self.player_token_to_game = {}

    def game_by_id(self, game_id: int) -> Game:
        with self.lock:
            return self.games[game_id]

    def game_by_token(self, token: str) -> Game:
        with self.lock:
            return self.player_token_to_game[token]

    def player_by_token(self, token: str) -> Player:
        with self.lock:
            return self.player_token_to_player[token]

    def new_game(self) -> int:
        with self.lock:
            game_id = self.next_game_id
            self.games[game_id] = Game(game_id)
            self.next_game_id += 1

            return game_id

    def add_player(self, game: Game, player_name: str, token: str) -> Player:
        with self.lock:
            player = game.add_player(player_name, token)
            self.player_token_to_player[token] = player
            self.player_token_to_game[token] = game

            return player

    def run_periodic_cleanup(self) -> None:
        while True:  # currently no "clean exit" capability
            with self.lock:
                # take a copy to avoid editing something we're iterating over
                for game in list(self.games.values()):
                    if datetime.datetime.now() - game.last_modified_at > datetime.timedelta(
                        minutes=GAME_TIMEOUT_MINUTES
                    ):
                        self._cleanup(game)

            time.sleep(60)

    def _cleanup(self, game: Game) -> None:
        # Don't worry about not-found errors as we're deleting the game and players anyway
        for player in game.players:
            self.player_token_to_game.pop(player.token, None)
            self.player_token_to_player.pop(player.token, None)

        self.games.pop(game.id, None)


class HanapiServer:
    app: Flask
    game_database: GameDatabase
    cleanup_thread: Thread

    def __init__(self, app: Optional[Flask] = None) -> None:
        """
        app is passed if running from the WSGI wrapper script.
        Otherwise, app will be None and we make one in here
        (and start it ourselves in debug mode, since we are likely running on a dev box).
        """
        self.app = app if app else Flask("hanapi")
        self.game_database = GameDatabase()

        # Game setup
        self.app.add_url_rule(
            "/games", methods=["GET", "POST"], view_func=self.create_or_list_games
        )
        self.app.add_url_rule(
            "/games/<game_id_str>/players",
            methods=["GET", "POST"],
            view_func=self.add_or_list_players,
        )

        # Game logic (requires token to identify player and game)
        self.app.add_url_rule("/start", methods=["POST"], view_func=self.start_game)
        self.app.add_url_rule("/abort", methods=["POST"], view_func=self.abort_game)
        self.app.add_url_rule("/status", methods=["GET"], view_func=self.game_status)
        self.app.add_url_rule("/cards", methods=["GET"], view_func=self.game_cards)
        self.app.add_url_rule(
            "/cards/<card_index_str>", methods=["PUT", "DELETE"], view_func=self.play_or_discard
        )
        self.app.add_url_rule(
            "/knowledge/<target_player_name>", methods=["PUT"], view_func=self.give_knowledge
        )

        # Start cleanup thread
        self.cleanup_thread = Thread(target=self.game_database.run_periodic_cleanup)
        self.cleanup_thread.start()

        # Start server (when launching from WSGI, this will be done for us)
        if not app:
            self.app.run(debug=True, use_reloader=False)

    @staticmethod
    def _format_game_status(game: Game) -> str:
        if game.status is GameStatus.WAITING_TO_START:
            return f"⏳️ Game is {game.status.value}.\n"

        if game.status in [GameStatus.WON, GameStatus.LOST]:
            emoji = "✅️" if game.status is GameStatus.WON else "❎️"
            return f"{emoji} Game was {game.status.value}, with a score of {game.score}.\n"

        if game.status is GameStatus.ABORTED:
            # aborted
            return f"⛔️ Game was {game.status.value}.\n"

        # game is in progress
        assert game.status is GameStatus.IN_PROGRESS

        foundation_line = ", ".join(
            f"{colour.value}: {number}" for colour, number in game.foundation.items()
        )
        if game.deck:
            deck_line = (
                f"The deck has {len(game.deck)} card{'' if len(game.deck) == 1 else 's'} remaining."
            )
        else:
            deck_line = "The deck is empty. " + (
                f"There are {game.turns_remaining} turns remaining."
                if game.turns_remaining != 1
                else "This is the final turn."
            )

        # something like:
        # There are 2 knowledge tokens available.
        # There is 1 knowledge token available.
        # There are no knowledge tokens available.
        knowledge_tokens_line = (
            f"There {'is' if game.knowledge_tokens == 1 else 'are'} "
            f"{'no' if game.knowledge_tokens == 0 else game.knowledge_tokens} "
            f"knowledge token{'' if game.knowledge_tokens == 1 else 's'} available."
        )

        return textwrap.dedent(
            f"""\
            Game is in progress. It is {game.current_player.name}'s turn.
            The foundation is currently: {foundation_line}
            {deck_line}
            {knowledge_tokens_line}
            The players have {game.lives} {'life' if game.lives == 1 else 'lives'} left.
            """
        )

    @staticmethod
    def _format_discard_pile(game: Game) -> str:
        if game.status is not GameStatus.IN_PROGRESS:
            return ""

        if not game.discard_pile:
            return "The discard pile is empty."

        lines = ["The discard pile contains:"]

        # get a mapping of card colour to {number: how many of that number}
        cards_by_colour: dict = {colour: defaultdict(int) for colour in CardColour}

        for card in game.discard_pile:
            cards_by_colour[card.colour][card.number] += 1

        for colour, count_of_number in cards_by_colour.items():
            per_colour_text = []

            for number in range(1, 6):
                if number in count_of_number:
                    count = count_of_number[number]
                    english_count = ["", "a", "two", "three"][count]
                    # this gives something like "two 3s"
                    per_colour_text.append(f"{english_count} {number}{'s' if count > 1 else ''}")

            if per_colour_text:
                # have at least one card of this colour, so add it to the output
                # this gives something like "  - in yellow, two 3s and a 4"
                lines.append(f"  - in {colour.value}, {format_list(per_colour_text, 'and')}")

        return "\n".join(lines) + ".\n"

    @staticmethod
    def _format_cards(player: Player, knowledge_only: bool) -> str:
        def format_non_knowledge_list(non_knowledge: list[str]) -> str:
            return format_list(sorted(non_knowledge), "or")

        def format_non_knowledge_colours(knowledge: CardKnowledge) -> str:
            return format_non_knowledge_list(
                [colour.value for colour in knowledge.known_non_colours]
            )

        def format_non_knowledge_numbers(knowledge: CardKnowledge) -> str:
            return format_non_knowledge_list(
                [f"a {number}" for number in knowledge.known_non_numbers]
            )

        lines: list[str] = []
        for index, card_with_knowledge in enumerate(player.cards):
            if card_with_knowledge is None:
                continue

            k = card_with_knowledge.knowledge
            if knowledge_only:
                if k.number and k.colour:
                    line = f"a {k.colour.value} {k.number}"
                elif k.number and k.known_non_colours:
                    line = f"a {k.number}, which is not {format_non_knowledge_colours(k)}"
                elif k.number:
                    line = f"a {k.number}, where you know no information about the colour"
                elif k.colour and k.known_non_numbers:
                    line = (
                        f"a {k.colour.value} card, which is not "
                        f"{format_non_knowledge_numbers(k)}"
                    )
                elif k.colour:
                    line = (
                        f"a {k.colour.value} card, where you know no information about the number"
                    )
                elif k.known_non_numbers or k.known_non_colours:
                    if k.known_non_numbers and k.known_non_colours:
                        non_knowledge = (
                            f"{format_non_knowledge_colours(k)}; "
                            f"nor {format_non_knowledge_numbers(k)}"
                        )
                    elif k.known_non_colours:
                        non_knowledge = format_non_knowledge_colours(k)
                    else:
                        non_knowledge = format_non_knowledge_numbers(k)

                    line = f"a card which is not {non_knowledge}"
                else:
                    line = "a card about which you have no information"

                lines.append(f"  [index {str(index):>2s}] {line}")

            else:
                lines.append(
                    f"  a {card_with_knowledge.card.colour.value} {card_with_knowledge.card.number}"
                )

                # Also inform the other players what the player holding the card knows
                if k.number and k.colour:
                    lines.append("    - they know this")
                elif k.number and k.known_non_colours:
                    lines.append(
                        f"    - they know it is a {k.number}, "
                        f"and not {format_non_knowledge_colours(k)}"
                    )
                elif k.number:
                    lines.append(
                        f"    - they know it is a {k.number}, but nothing about the colour"
                    )
                elif k.colour and k.known_non_numbers:
                    lines.append(
                        f"    - they know it is a {k.colour.value} card, "
                        f"and not {format_non_knowledge_numbers(k)}"
                    )
                elif k.colour:
                    lines.append(
                        f"    - they know it is a {k.colour.value} card, "
                        "but nothing about the number"
                    )
                elif k.known_non_numbers or k.known_non_colours:
                    if k.known_non_numbers and k.known_non_colours:
                        non_knowledge = (
                            f"{format_non_knowledge_colours(k)}; "
                            f"nor {format_non_knowledge_numbers(k)}"
                        )
                    elif k.known_non_colours:
                        non_knowledge = format_non_knowledge_colours(k)
                    else:
                        non_knowledge = format_non_knowledge_numbers(k)

                    lines.append(f"    - they know it is not {non_knowledge}")
                else:
                    lines.append("    - they have no information about this card")

        return "\n".join(lines) + ".\n"

    @staticmethod
    def _format_all_cards(game: Game, source: Player) -> str:
        blocks = []
        for player in game.players:
            if player is source:
                blocks.append("You hold:\n" + HanapiServer._format_cards(player, True))
            else:
                blocks.append(f"{player.name} holds:\n" + HanapiServer._format_cards(player, False))

        blocks.append(HanapiServer._format_discard_pile(game))

        return "\n".join(blocks)

    def _run_general_game_func_requiring_token(
        self, callback: Callable[[Game, Player], HttpReturnType]
    ) -> HttpReturnType:
        try:
            token = request.headers["Authorization"]
        except KeyError:
            return "⚠️ Please pass token on the Authorization header.\n", 401

        try:
            player = self.game_database.player_by_token(token)
        except KeyError:
            return "⚠️ Token not recognized.\n", 404

        try:
            game = self.game_database.game_by_token(token)
        except KeyError:
            return (
                "⚠️ Game not found. "
                f"Perhaps it finished {GAME_TIMEOUT_MINUTES} or more minutes ago?\n"
            ), 404

        try:
            return callback(game, player)
        except (
            RuntimeError,
            KeyError,
            ValueError,
            IndexError,
            InvalidMoveError,
            OutOfTokensError,
        ) as e:
            if isinstance(e, KeyError):
                # https://bugs.python.org/issue2651
                # Python formats KeyErrors with quotes, whereas other exceptions don't get quotes.
                formatted_exception = str(e).strip('"')
            else:
                formatted_exception = str(e)

            return f"❗️ Error: {formatted_exception}.\n", 400

    #
    # API endpoints
    #

    def create_or_list_games(self) -> HttpReturnType:
        if request.method == "POST":
            game_id = self.game_database.new_game()

            return (
                f"✨️ Created game {game_id}.\n"
                f"POST /games/{game_id}/players?name=<name> to add a player.\n"
                f"Then have a player POST /start (with token) to start the game.\n"
            ), 201

        # should be GET - list all games
        lines = []
        games = list(self.game_database.games.values())

        if games:
            for game in games:
                last_modified_minutes = floor(
                    (datetime.datetime.now() - game.last_modified_at).total_seconds() / 60
                )
                last_modified_str = (
                    "less than a minute ago"
                    if last_modified_minutes <= 0
                    else f"{last_modified_minutes} minutes ago"
                )
                lines.append(
                    f"- Game {game.id} is {game.status.value}, last modified {last_modified_str}."
                )

            return "\n".join(lines) + "\n", 200

        return "There are no games.\n", 200

    def add_or_list_players(self, game_id_str: str) -> HttpReturnType:
        try:
            game_id = int(game_id_str)
        except ValueError:
            return f"⚠️ Invalid game ID {game_id_str}.\n", 400

        try:
            game = self.game_database.game_by_id(game_id)
        except KeyError:
            return f"⚠️ Game {game_id} not found.\n", 404

        if request.method == "POST":
            try:
                player_name = request.args["name"]
            except KeyError:
                return "⚠️ Parameter 'name' (player name) is mandatory.\n", 400

            token = token_hex()
            other_players_line = (
                (
                    "Other players in the game are: "
                    f"{format_list([player.name for player in game.players], 'and')}."
                )
                if game.players
                else "You are the first player to join the game."
            )

            try:
                self.game_database.add_player(game, player_name, token)
            except (RuntimeError, ValueError) as e:
                # RuntimeError: likely cause being that the game has already started
                # ValueError: something wrong with the username (already taken, or invalid chars)
                return f"❗️ Error: {e}.\n", 400

            return (
                textwrap.dedent(
                    f"""\
                    👋️ Welcome, {player_name}! Added you to game {game_id}.
                    {other_players_line}
                    Your token is {token} -
                    pass this in the Authorization header in any request relating to the game.
                    """
                ),
                201,
            )

        return ", ".join([player.name for player in game.players]), 200

    def start_game(self) -> HttpReturnType:
        def do_start_game(game: Game, _player: Player) -> HttpReturnType:
            try:
                game.start()
            except RuntimeError as e:  # game already started, not enough players, etc.
                return f"❗️ Error: {e}.\n", 400

            return (
                f"🏁️ Game {game.id} has started!\n"
                f"Query the game status with a GET /status and GET /cards.\n"
                f"On your turn, you can call one of the following:\n"
                f"- PUT /cards/<index> to play a card\n"
                f"- DELETE /cards/<index> to discard a card\n"
                f"- PUT /knowledge/<player name>?colour=<colour> "
                f"or ?number=<number> to give a player info.\n"
            ), 200

        return self._run_general_game_func_requiring_token(do_start_game)

    def abort_game(self) -> HttpReturnType:
        def do_abort_game(game: Game, _player: Player) -> HttpReturnType:
            game.abort()

            return f"⛔️ Game {game.id} was aborted.\n", 200

        return self._run_general_game_func_requiring_token(do_abort_game)

    def game_status(self) -> HttpReturnType:
        def do_game_status(game: Game, _player: Player) -> HttpReturnType:
            return self._format_game_status(game), 200

        return self._run_general_game_func_requiring_token(do_game_status)

    def game_cards(self) -> HttpReturnType:
        def do_game_cards(game: Game, player: Player) -> HttpReturnType:
            return self._format_all_cards(game, player), 200

        return self._run_general_game_func_requiring_token(do_game_cards)

    def play_or_discard(self, card_index_str: str) -> HttpReturnType:
        try:
            card_index = int(card_index_str)
        except ValueError:
            return f"⚠️ Invalid card index {card_index_str}.\n", 400

        def do_play(game: Game, player: Player) -> HttpReturnType:
            card, successful_play = game.run_turn_play(player, card_index)
            if successful_play:
                return f"🎉️ The card - a {card} - was played to the foundation successfully!\n", 200

            # still a valid request, processed successfully,
            # even if it wasn't a success in-game - so return 200
            return f"😿️ The card - a {card} - could not be played. The team loses a life.\n", 200

        def do_discard(game: Game, player: Player) -> HttpReturnType:
            card = game.run_turn_discard(player, card_index)
            return f"🚮️ The card - a {card} - was discarded.\n", 200

        if request.method == "PUT":
            return self._run_general_game_func_requiring_token(do_play)

        return self._run_general_game_func_requiring_token(do_discard)

    def give_knowledge(self, target_player_name: str) -> HttpReturnType:
        try:
            colour_str: Optional[str] = request.args["colour"]
        except KeyError:
            colour_str = None

        if colour_str:
            try:
                colour: Optional[CardColour] = CardColour(colour_str)
            except ValueError:
                return f"⚠️ Invalid colour {colour_str}.\n", 400
        else:
            colour = None

        try:
            number_str: Optional[str] = request.args["number"]
        except KeyError:
            number_str = None

        if number_str:
            try:
                number: Optional[int] = int(number_str)
            except ValueError:
                return f"⚠️ Invalid number {number_str}.\n", 400
        else:
            number = None

        def do_give_knowledge(game: Game, player: Player) -> HttpReturnType:
            game.run_turn_knowledge(player, target_player_name, colour, number)

            return (
                f"💬️ You told {target_player_name} about cards of "
                + (f"{colour.value} colour" if colour else f"the number {number}")
                + ".\n",
                200,
            )

        return self._run_general_game_func_requiring_token(do_give_knowledge)


SERVER: HanapiServer


def main() -> None:
    # pylint: disable=global-statement
    global SERVER
    SERVER = HanapiServer()


def format_list(the_list: list[str], conjunction: str) -> str:
    if len(the_list) == 1:
        return the_list[0]

    return ", ".join(the_list[:-1]) + f" {conjunction} {the_list[-1]}"


if __name__ == "__main__":
    main()
