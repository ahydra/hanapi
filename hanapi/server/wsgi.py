from functools import partial
from typing import Any, Callable

from flask import Flask

from hanapi.server.server import HanapiServer


def wsgi_entrypoint_wrapper(the_app: Flask, env: Any, start_response: Callable) -> Any:
    return the_app(env, start_response)


app = Flask("hanapi")
_server = HanapiServer(app)

# Expose entrypoint with env and start_response parameters, which calls into our app
app.wsgi_app = partial(wsgi_entrypoint_wrapper, app.wsgi_app)  # type: ignore
WSGI_ENTRYPOINT: Flask = app.wsgi_app
