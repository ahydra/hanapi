import datetime
from enum import Enum
import random
import string
from typing import Optional

import attr

from hanapi.dataclasses.card import Card, CardColour, CardKnowledge, CardWithKnowledge
from hanapi.dataclasses.exceptions import InvalidMoveError, OutOfTokensError


class GameStatus(Enum):
    WAITING_TO_START = "waiting to start"
    IN_PROGRESS = "in progress"
    WON = "won"
    LOST = "lost"
    ABORTED = "aborted"


MAX_KNOWLEDGE_TOKENS = 8


@attr.s(auto_attribs=True)
class Player:
    name: str
    token: str
    # to help players keep track, just add new cards and set played/discarded ones to None;
    # that way the cards keep their indices throughout the game
    cards: list[Optional[CardWithKnowledge]] = attr.ib(factory=list)


class Game:
    id: int
    players: list[Player]
    deck: list[Card]
    foundation: dict[CardColour, int]
    discard_pile: list[Card]
    knowledge_tokens: int
    lives: int
    turns_remaining: Optional[int]  # only set when in the last phase of the game (deck empty)
    status: GameStatus
    current_turn_index: int  # index into self.players indicating whose turn it is
    last_modified_at: datetime.datetime

    def __init__(self, game_id: int) -> None:
        self.id = game_id
        self.players = []
        self.last_modified_at = datetime.datetime.now()
        self.status = GameStatus.WAITING_TO_START

    def _touch(self) -> None:  # somewhat inappropriately suggestive?
        self.last_modified_at = datetime.datetime.now()

    def _end(self, status: GameStatus) -> None:
        self.status = status
        self._touch()

    def _next_turn(self) -> None:
        self.current_turn_index = (self.current_turn_index + 1) % len(self.players)

        if self.turns_remaining is not None:
            self.turns_remaining -= 1
            if self.turns_remaining == 0:
                self._end(GameStatus.WON)

    def _draw_to_replace(self, player: Player, card_index: int) -> None:
        player.cards[card_index] = None

        if not self.deck:
            return

        player.cards.append(CardWithKnowledge(self.deck.pop(), CardKnowledge()))

        if not self.deck:
            # Deck is now exhausted, game will end when all players have had one more turn.
            # The call to _next_turn is always made after this, so add one to compensate.
            self.turns_remaining = len(self.players) + 1

    def abort(self) -> None:
        # avoid people keeping the game alive infinitely by aborting it repeatedly
        if self.status != GameStatus.ABORTED:
            self._end(GameStatus.ABORTED)
            self._touch()

    def add_player(self, player_name: str, player_token: str) -> Player:
        if self.status is not GameStatus.WAITING_TO_START:
            raise RuntimeError("Game has already started, or was aborted")

        if len(self.players) >= 5:
            raise RuntimeError("Game is full")

        allowed_username_chars = string.ascii_letters + string.digits + "_"

        if any(char not in allowed_username_chars for char in player_name):
            raise ValueError("Username must contain only alphanumeric characters and underscores")

        if len(player_name) < 3 or len(player_name) > 15:
            raise ValueError("Username must be between 3 and 15 characters (inclusive) in length")

        if any(player.name == player_name for player in self.players):
            raise ValueError("Player name already in use")

        new_player = Player(player_name, player_token)
        self.players.append(new_player)
        self._touch()
        return new_player

    def start(self) -> None:
        if self.status is not GameStatus.WAITING_TO_START:
            raise RuntimeError("Game has already started, or was aborted")

        if len(self.players) < 2:
            raise RuntimeError("Need at least 2 players")

        self.deck = [
            Card(colour, number)
            for colour in CardColour
            for number in (1, 1, 1, 2, 2, 3, 3, 4, 4, 5)
        ]
        random.seed()

        # Knuth-shuffle the deck
        i = len(self.deck) - 1
        while i > 0:  # don't include i = 0 (only option is to shuffle it with itself)
            j = random.randint(0, i)
            temp = self.deck[i]
            self.deck[i] = self.deck[j]
            self.deck[j] = temp

            i -= 1

        # Deal cards to each player
        cards_per_player = 5 if len(self.players) in [2, 3] else 4
        for player in self.players:
            for _ in range(cards_per_player):
                card = self.deck.pop()
                player.cards.append(CardWithKnowledge(card, CardKnowledge()))

        self.knowledge_tokens = MAX_KNOWLEDGE_TOKENS
        self.lives = 3
        self.foundation = {colour: 0 for colour in CardColour}
        self.discard_pile = []
        self.status = GameStatus.IN_PROGRESS
        self.current_turn_index = random.randrange(0, len(self.players))
        self.turns_remaining = None
        self._touch()

    @property
    def current_player(self) -> Player:
        return self.players[self.current_turn_index]

    @property
    def score(self) -> int:
        return sum(self.foundation.values())

    def run_turn_knowledge(
        self,
        source: Player,
        target_player_name: str,
        colour: Optional[CardColour] = None,
        number: Optional[int] = None,
    ) -> None:
        if self.status is not GameStatus.IN_PROGRESS:
            raise RuntimeError("Game not in progress")

        if source is not self.current_player:
            raise InvalidMoveError("It's not your turn")

        targets = [player for player in self.players if player.name == target_player_name]
        if not targets:
            raise KeyError(f"Unknown player name '{target_player_name}'")

        assert len(targets) == 1
        target = targets[0]

        if source == target:
            raise InvalidMoveError("Can't tell yourself about your own cards")

        if colour is None and number is None:
            raise InvalidMoveError("Need something to tell about")

        if colour and number:
            raise InvalidMoveError("Can only tell about colour or number; not both")

        if number and (number < 1 or number > 5):
            raise InvalidMoveError("Number must be between 1 and 5")

        if self.knowledge_tokens == 0:
            raise OutOfTokensError("No knowledge tokens left")

        for card_with_knowledge in target.cards:
            if card_with_knowledge is None:
                continue

            if colour:
                if card_with_knowledge.card.colour == colour:
                    # This card is now known to be of the given colour
                    card_with_knowledge.knowledge.colour = colour
                else:
                    # This card is now known to NOT be of the given colour
                    card_with_knowledge.knowledge.known_non_colours.add(colour)
            else:
                assert number

                if card_with_knowledge.card.number == number:
                    # This card is now known to be of the given number
                    card_with_knowledge.knowledge.number = number
                else:
                    # This card is now known to NOT be of the given number
                    card_with_knowledge.knowledge.known_non_numbers.add(number)

        self.knowledge_tokens -= 1
        self._next_turn()
        self._touch()

    def run_turn_play(self, source: Player, card_index: int) -> tuple[Card, bool]:
        if self.status is not GameStatus.IN_PROGRESS:
            raise RuntimeError("Game not in progress")

        if source is not self.current_player:
            raise InvalidMoveError("It's not your turn")

        if card_index < 0 or card_index > len(source.cards):
            raise IndexError("Invalid card index")

        if (card_with_knowledge := source.cards[card_index]) is None:
            raise InvalidMoveError("Card has already been played/discarded")

        # Can the card be played?
        target = self.foundation[card_with_knowledge.card.colour] + 1
        rc = card_with_knowledge.card.number == target
        if rc:
            # Yes!
            self.foundation[card_with_knowledge.card.colour] = card_with_knowledge.card.number

            if (
                card_with_knowledge.card.number == 5
                and self.knowledge_tokens < MAX_KNOWLEDGE_TOKENS
            ):
                # Extra knowledge point for completing a stack
                self.knowledge_tokens += 1

            if all(built == 5 for built in self.foundation.values()):
                # The players got the perfect score! :-) No point in playing on.
                self._end(GameStatus.WON)

        else:
            # No - lose a life and discard the card
            self.lives -= 1
            self.discard_pile.append(card_with_knowledge.card)

            if self.lives == 0:
                # The players lost :-(
                self._end(GameStatus.LOST)

        self._draw_to_replace(source, card_index)
        self._next_turn()
        self._touch()

        return card_with_knowledge.card, rc

    def run_turn_discard(self, source: Player, card_index: int) -> Card:
        if self.status is not GameStatus.IN_PROGRESS:
            raise RuntimeError("Game not in progress")

        if source is not self.current_player:
            raise InvalidMoveError("It's not your turn")

        if card_index < 0 or card_index > len(source.cards):
            raise IndexError("Invalid card index")

        if (card_with_knowledge := source.cards[card_index]) is None:
            raise InvalidMoveError("Card has already been played/discarded")

        if self.knowledge_tokens == MAX_KNOWLEDGE_TOKENS:
            raise OutOfTokensError("Cannot discard when all knowledge tokens are available")

        self.discard_pile.append(card_with_knowledge.card)
        self._draw_to_replace(source, card_index)
        self.knowledge_tokens += 1
        self._next_turn()
        self._touch()

        return card_with_knowledge.card
