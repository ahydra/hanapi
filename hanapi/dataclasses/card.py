from enum import Enum
from typing import Optional

import attr


class CardColour(Enum):
    RED = "red"
    GREEN = "green"
    BLUE = "blue"
    YELLOW = "yellow"
    BLACK = "black"


@attr.s(auto_attribs=True, frozen=True)
class Card:
    colour: CardColour
    number: int  # 1 to 5

    def __str__(self) -> str:
        return f"{self.colour.value} {self.number}"


@attr.s(auto_attribs=True)
class CardKnowledge:
    colour: Optional[CardColour] = None
    known_non_colours: set[CardColour] = attr.ib(factory=set)
    number: Optional[int] = None
    known_non_numbers: set[int] = attr.ib(factory=set)


@attr.s(auto_attribs=True, frozen=True)
class CardWithKnowledge:
    card: Card
    knowledge: CardKnowledge
