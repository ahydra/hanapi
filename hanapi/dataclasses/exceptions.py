class InvalidMoveError(Exception):
    pass


class OutOfTokensError(Exception):
    pass
