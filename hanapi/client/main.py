import argparse
from enum import Enum
import re
import sys
import time
from typing import Callable, NoReturn

import requests


class CommandResult(Enum):
    HANDLED_STOP = "stop"  # command handled, no more commands required
    HANDLED_CONTINUE = "continue"  # command handled but more commands required
    USAGE_ERROR = "usage error"  # invalid usage
    COMMAND_ERROR = "command error"  # error on handling command, try again


class HanapiClient:
    def __init__(self, server_url: str, game_id: int, username: str, hosting: bool = False) -> None:
        self._server_url = server_url
        self._game_id = game_id
        self._username = username
        self._hosting = hosting

        # Join the game
        rsp = requests.post(server_url + f"games/{game_id}/players?name={username}")

        if (rsp.status_code == 201) and (
            matches := re.search(r"Your token is ([a-f0-9]{64})", rsp.text)
        ):
            print("Successfully joined the game.")
            self._token = str(matches.group(1))
        else:
            raise RuntimeError(f"Failed to join game. Response was {rsp.text}")

    @staticmethod
    def run_command_loop(commands: dict[str, Callable[[list[str]], CommandResult]]) -> None:
        available_commands = f"Available commands: {', '.join(commands.keys())}"

        while True:
            print()
            command = input("Enter command (use 'help' to see commands): ").strip()

            if not command:
                print(available_commands + "\n")
                continue

            parts = command.split(" ")
            verb = parts[0]

            if verb == "help":
                print(available_commands + "\n")
                continue

            if verb not in commands:
                print(f"Unrecognized command. {available_commands}\n")
                continue

            result = commands[verb](parts[1:])
            if result is CommandResult.HANDLED_STOP:
                # handled OK, break out of the loop
                return

            if result is CommandResult.HANDLED_CONTINUE:
                # we need another command, so go round again
                continue

            if result is CommandResult.USAGE_ERROR:
                # command params were invalid
                continue

            if result is CommandResult.COMMAND_ERROR:
                # command didn't work, try again
                continue

            assert False

    def _do_token_request(
        self, method: Callable[..., requests.Response], endpoint: str
    ) -> requests.Response:
        rsp = method(self._server_url + endpoint, headers={"Authorization": self._token})
        return rsp

    # Command handlers

    def handle_abort(self, params: list[str]) -> CommandResult:
        if params:
            print("Command 'abort' does not take any parameters.")
            return CommandResult.USAGE_ERROR

        rsp = self._do_token_request(requests.post, "abort")
        print(rsp.text)
        return CommandResult.HANDLED_STOP

    def handle_status(self, params: list[str]) -> CommandResult:
        if params:
            print("Command 'status' does not take any parameters.")
            return CommandResult.USAGE_ERROR

        rsp = self._do_token_request(requests.get, "status")
        print(rsp.text)
        return CommandResult.HANDLED_CONTINUE

    def handle_status_with_cards(self, params: list[str]) -> CommandResult:
        if params:
            print("Command 'status' does not take any parameters.")
            return CommandResult.USAGE_ERROR

        rsp = self._do_token_request(requests.get, "status")
        print(rsp.text)
        rsp = self._do_token_request(requests.get, "cards")
        print(rsp.text)
        return CommandResult.HANDLED_CONTINUE

    def handle_players(self, params: list[str]) -> CommandResult:
        if params:
            print("Command 'players' does not take any parameters.")
            return CommandResult.USAGE_ERROR

        rsp = requests.get(f"{self._server_url}/games/{self._game_id}/players")
        print(f"Players in the game: {rsp.text}")
        return CommandResult.HANDLED_CONTINUE

    def handle_start(self, params: list[str]) -> CommandResult:
        if params:
            print("Command 'start' does not take any parameters.")
            return CommandResult.USAGE_ERROR

        rsp = self._do_token_request(requests.post, "start")
        print(rsp.text)

        return CommandResult.HANDLED_STOP if rsp.status_code == 200 else CommandResult.COMMAND_ERROR

    def handle_play(self, params: list[str]) -> CommandResult:
        if len(params) != 1:
            print("Command 'play' takes one parameter: the index of the card to play.")
            return CommandResult.USAGE_ERROR

        rsp = self._do_token_request(requests.put, f"cards/{params[0]}")
        print(rsp.text)

        return CommandResult.HANDLED_STOP if rsp.status_code == 200 else CommandResult.COMMAND_ERROR

    def handle_discard(self, params: list[str]) -> CommandResult:
        if len(params) != 1:
            print("Command 'discard' takes one parameter: the index of the card to discard.")
            return CommandResult.USAGE_ERROR

        rsp = self._do_token_request(requests.delete, f"cards/{params[0]}")
        print(rsp.text)

        return CommandResult.HANDLED_STOP if rsp.status_code == 200 else CommandResult.COMMAND_ERROR

    def handle_info(self, params: list[str]) -> CommandResult:
        if (len(params) != 3) or (params[1] not in ["colour", "number"]):
            print(
                "Command 'info' takes three parameters: "
                "<player name> <'colour' or 'number'> <value>"
            )
            print("  e.g. info bob colour red, or info alice number 3")
            return CommandResult.USAGE_ERROR

        rsp = self._do_token_request(requests.put, f"knowledge/{params[0]}?{params[1]}={params[2]}")
        print(rsp.text)

        return CommandResult.HANDLED_STOP if rsp.status_code == 200 else CommandResult.COMMAND_ERROR

    def run_hosting_loop(self) -> None:
        self.run_command_loop(
            {
                "status": self.handle_status,
                "players": self.handle_players,
                "abort": self.handle_abort,
                "start": self.handle_start,
            }
        )

    def run_non_hosting_loop(self) -> None:
        current_players_in_game: set[str] = set()
        game_started = False
        loops = 0

        while not game_started:
            rsp = requests.get(f"{self._server_url}/games/{self._game_id}/players")
            if rsp.status_code != 200:
                raise RuntimeError(f"Failed to query list of players. Response was {rsp.text}")

            new_players_in_game = set(rsp.text.split(", "))
            if not current_players_in_game:
                print(f"Players in the game: {rsp.text}")

            elif new_players_in_game != current_players_in_game:
                for player in new_players_in_game - current_players_in_game:
                    print(f"{player} joined the game.")
                    loops = 0

            current_players_in_game = new_players_in_game

            rsp = self._do_token_request(requests.get, "status")
            if rsp.status_code != 200:
                raise RuntimeError(f"Failed to query game status. Response was {rsp.text}")

            game_started = "Game is in progress" in rsp.text
            if (not game_started) and "waiting to start" not in rsp.text:
                # game was aborted, or something bad happened
                print(rsp.text)
                return

            if loops:
                print("\x1B[1A\x1B[2K", end="")  # move cursor up one line and erase line

            loops = (loops % 3) + 1

            print("Waiting for the game to start" + ("." * loops))
            time.sleep(3)

    def run_game_loop(self) -> None:
        current_turn = ""
        loops = 0

        while True:
            rsp = self._do_token_request(requests.get, "status")
            if rsp.status_code != 200:
                raise RuntimeError(f"Failed to query game status. Response was {rsp.text}")

            if matches := re.search(r"Game is in progress. It is (.*?)'s turn.", rsp.text):
                new_turn = matches.group(1)
            else:
                # game finished, probably
                print(rsp.text)
                break

            if current_turn != new_turn:
                current_turn = new_turn
                if current_turn == self._username:
                    # It's our turn! Print the status and cards, then see what we should do
                    print(rsp.text)

                    rsp = self._do_token_request(requests.get, "cards")
                    if rsp.status_code != 200:
                        raise RuntimeError(f"Failed to query cards. Response was {rsp.text}")

                    print(rsp.text)

                    print("It is your turn!")

                    self.run_command_loop(
                        {
                            "status": self.handle_status_with_cards,
                            "play": self.handle_play,
                            "discard": self.handle_discard,
                            "info": self.handle_info,
                        }
                        | ({"abort": self.handle_abort} if self._hosting else {})  # type: ignore
                    )

                    # Don't enter the loop that waits for us to finish our turn,
                    # as we just finished it!
                    continue

                # Change of turns, but still not our turn.
                # Wait again until this turn is finished.
                print()
                print(rsp.text)
                print()
                loops = 0

            if loops:
                print("\x1B[1A\x1B[2K", end="")  # move cursor up one line and erase line

            loops = (loops % 3) + 1

            print(f"Waiting for {current_turn} to take their turn" + ("." * loops))
            time.sleep(3)

    def run(self) -> None:
        if self._hosting:
            self.run_hosting_loop()
        else:
            self.run_non_hosting_loop()

        # Don't bother trying to play the game if it wasn't started successfully
        rsp = self._do_token_request(requests.get, "status")
        if rsp.status_code != 200:
            raise RuntimeError(f"Failed to query game status. Response was {rsp.text}")

        if "in progress" in rsp.text:
            self.run_game_loop()


def main(args: argparse.Namespace) -> int:
    server_url = f"http://{args.server}:{args.port}/"
    username = args.username

    if args.create_game:
        rsp = requests.post(server_url + "games")
        rsp.raise_for_status()

        if matches := re.search(r"Created game (\d+)", rsp.text):
            game_id = int(matches.group(1))
            print(f"Created game. Share this game ID with fellow players: {game_id}")
        else:
            raise RuntimeError(f"Failed to create game. Response was {rsp.text}")
    else:
        game_id = args.game_id

    try:
        client = HanapiClient(server_url, game_id, username, args.create_game)
        client.run()
        return 0
    except RuntimeError as e:
        print(f"Encountered fatal error: {e}")
        return 1


def entrypoint() -> NoReturn:
    sys.exit(main(parse_args()))


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="HanAPI client")

    parser.add_argument("--server", required=True, help="IP or hostname of server")
    parser.add_argument("--port", default=80, type=int, help="server port")
    parser.add_argument("--username", required=True, help="your username")

    game_group = parser.add_mutually_exclusive_group(required=True)
    game_group.add_argument("--create-game", action="store_true", help="create the game")
    game_group.add_argument("--game-id", type=int, help="game ID to join")

    return parser.parse_args()


if __name__ == "__main__":
    entrypoint()
