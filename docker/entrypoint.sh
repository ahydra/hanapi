#!/bin/bash

/docker-entrypoint.sh nginx -g "daemon off;" &
su www -c "cd hanapi && /home/www/.local/bin/poetry run uwsgi --ini hanapi/server/wsgi.ini"
