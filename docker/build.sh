#!/bin/bash

TEMPDIR=$(mktemp -d)
VERSION=$(grep version pyproject.toml | cut -d'"' -f 2)

cp -R hanapi ${TEMPDIR}
find ${TEMPDIR}/hanapi -type d -name __pycache__ -exec rm -rf {} \;
cp poetry.lock pyproject.toml docker/Dockerfile docker/entrypoint.sh docker/hanapi.conf ${TEMPDIR}
docker build --tag hanapi:${VERSION} ${TEMPDIR} && rm -rf ${TEMPDIR}
