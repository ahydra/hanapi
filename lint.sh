# Show the diff, but also run a check to set the return value
FMT_CMD="--diff --check"

if (( $# == 1 ))
then
    if [[ $1 == "--fmt" ]]
    then
        FMT_CMD="" # no cmd = defaults to formatting files in-place
    elif [[ $1 == "--check" ]]
    then
        FMT_CMD=$1
    else
        usage
    fi
elif (( $# != 0 ))
then
    usage
fi

PYTHON_PATHS=(
    hanapi
)

rc=0

echo '*** Running isort ***'
poetry run isort ${FMT_CMD} "${PYTHON_PATHS[@]}"
rc=$((rc+$?))

echo '*** Running black ***'
poetry run black ${FMT_CMD} "${PYTHON_PATHS[@]}"
rc=$((rc+$?))

echo '*** Running mypy ***'
poetry run python3 -m mypy --pretty "${PYTHON_PATHS[@]}"
rc=$((rc+$?))

echo '*** Running pylint ***'
# "-j 0" option uses all available cores in parallel
poetry run python3 -m pylint -j 0 "${PYTHON_PATHS[@]}"
rc=$((rc+$?))

exit ${rc}
