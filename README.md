# HanAPI

A REST API-based implementation of the card game Hanabi.

## Setup

The project is built for Python 3.9 and uses Poetry.

- If you don't have them already installed,
  install `python3.9`, `python3.9-dev`, `gcc` and `python3-venv`
  using your package manager (e.g. `sudo apt install python3.9`).
- Install `poetry`: `curl -sSL https://install.python-poetry.org | python3 -`
  (or perhaps, to be safe, separate these commands so you can examine the script?!)
- Add `poetry`'s location to your `PATH`, as described in the output of the `poetry` install script.
  Restart shell / reload `.bashrc` if needed.
- Clone the HanAPI project.
- Within the project directory, run `poetry env use python3.9`, then `poetry install`.
- Test that the `poetry` env is working by running `./lint.sh`. It should report no errors.

## Server

### Production server

The server can be built into a Docker container.
It runs NGINX on port 80, forwarding the traffic to a uWSGI socket which is running the app.

To build the container:

- Ensure you have `docker` installed.
- Run `docker/build.sh`

To run the container:

- `docker run --net=host hanapi:0.1.0`
- It won't respond to `ctrl-C`, so you need to kill it using `docker kill <container name>`.

### Development server

To run a server locally in dev mode, use `poetry run start-server`.
It will listen on `localhost:5000`.

## Client

To start the client, use
`poetry run start-client --server <hostname or IP> [--port <port>] --username <username>`
with one of the two options below:

- If creating a game, add `--create-game`.
  It will tell you the game ID, which other players can then use to join the game.
- If joining a game, add `--game-id <id>`.

`port` defaults to 80 (the production server port).

The client is currently a primitive CLI.

## Roadmap

- Add a history API endpoint, so that the client can query this,
  and say "Alice discarded a red 4" or whatever
  to make it clear to the other players what other players did on their turns.
- Have the server return machine-readable JSON, and the client do the translation into English.
